const { isString, isArray, union, omit } = require("lodash");
const { Distribution, chanceSuccess, rollEquation } = require("@poisson-me/util-roll");

class ContentGenerator {
  constructor (content) {
    this.content = content;
  }

  getRule (grammar, name) {
    return this.content[grammar][name];
  }

  execute (grammar, origin) {
    let result = {};
    let command = { rule: origin };

    let iterator = new ExpansionIterator();
    iterator.add(command, result, grammar);

    let context = new ExecutionContext(
        this.getRule.bind(this),
        iterator.addAll.bind(iterator));

    while (iterator.hasNext()) {
        let item = iterator.next();
        this.expand(item.command, item.target, item.grammar, context)
    }

    return new Content(result);
  }

  parseRuleName (currentGrammar, rule) {
    return rule.includes(".")
      ? rule.split(".", 2)
      : [currentGrammar, rule];
  }

  expand (command, target, grammar, context) {
    if (command.chance && !chanceSuccess(command.chance)) {
      return; // Optional child didn't succeed on chance
    }

    var rule = command.rule;
    if (isString(rule)) {
      var paths = this.parseRuleName(grammar, rule);
      grammar = paths[0];
      rule = paths[1];
      rule = context.getRule(grammar, rule);
    }

    var targetKey = command.as ? command.as : command.rule;

    var count = command.count
      ? rollEquation(command.count)
      : 1;

    var collecting = isString(command.collect);

    var items = [];
    if (!collecting && (count > 1 || Array.isArray(rule))) {
      var distribution = new Distribution(command.distribution, rule);
      items = distribution.getN(count);
    } else {
      items = [rule];
    }

    var result = items.map(item => {
      var itemResult = null;
      if (isString(item)) {
        itemResult = item;
      } else {
        var localGrammar = grammar;
        if (item.type === "replacement") {
          var paths = this.parseRuleName(localGrammar, item.rule);
          localGrammar = paths[0];
          item = context.getRule(localGrammar, paths[1]);
        }
        itemResult = omit(item, "children", "weight", "chance", "as"); // copy of the rule
        if (isArray(item.children)) {
          context.addCommands(item.children, itemResult, localGrammar);
        }
      }
      if (collecting) {
        itemResult[command.collect] = count;
      }

      return itemResult;
    });

    // Keep a reference to child keys for future iteration
    var children = target._children || [];
    if (!children.includes(targetKey)) {
      children.push(targetKey);
      target._children = children;
    }

    var existingValue = target[targetKey];
    if (existingValue == null) {
      target[targetKey] = result.length === 1
        ? result[0]
        : result;
    } else {
      if (!isArray(existingValue)) {
        existingValue = [existingValue];
      }
      target[targetKey] = union(existingValue, result);
    }
  }
}

class ExpansionIterator {
  constructor() {
    this.commands = [];
  }

  add (command, target, grammar) {
    this.commands.push({
      command: command,
      target: target,
      grammar: grammar,
    });
  }

  addAll (commands, target, grammar) {
    commands.forEach(command => {
      this.add(command, target, grammar);
    });
  }

  hasNext () {
      return this.commands.length > 0;
  }

  next () {
      return this.commands.pop();
  }
}

class ExecutionContext {
  constructor(ruleSupplier, commandConsumer) {
    this.ruleSupplier = ruleSupplier;
    this.commandConsumer = commandConsumer;
  }

  getRule (grammar, name) {
      return this.ruleSupplier(grammar, name);
  }

  addCommands (commands, target, grammar) {
      this.commandConsumer(commands, target, grammar);
  }
}

class Content {
  constructor(raw) {
    this.raw = raw;
  }

  iterator () {
    return new ContentIterator(this.raw);
  }
}

class ContentIterator {
  constructor(root) {
    this.nodes = [];
    this.addChildren(root);
  }

  addChildren (node) {
    node.children.forEach((key) => {
      this.nodes.push(node[key]);
    });
  }

  hasNext () {
    return this.nodes.length > 0;
  }

  next () {
    let next = this.nodes.pop();
    this.addChildren(next);
    return next;
  }
}

module.exports = ContentGenerator;
