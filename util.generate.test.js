const test = require("tape");

const ContentGenerator = require("./util.generate");

test(function (t) {
  t.plan(1);

  t.ok(ContentGenerator, "generate is truthy");
}, "can import functions");

test(function (t) {
  let HelloWorldGrammar = {
    hello: {
      type: "hello",
      children: [
        { rule: "world", as: "who" },
      ],
    },
    world: {
      type: "world"
    }
  };
  let cg = new ContentGenerator({ HelloWorldGrammar });

  let result = cg.execute("HelloWorldGrammar", "hello");

  t.ok(result);
  t.deepLooseEqual(result.raw.hello, {
    type: "hello",
    _children: [ "who" ],
    who: {
      type: "world"
    }
  }, "generates content with aliases");

  t.end();
}, "simple content generation");
